package com.dockerdemo.deposit.counting;

import com.dockerdemo.deposit.domain.Deposit;
import com.dockerdemo.deposit.domain.OperationType;
import com.dockerdemo.deposit.domain.Transaction;
import com.dockerdemo.deposit.error.BadRequestException;

import java.util.Date;

public class TransactionOperations {

    public static Transaction createTransaction(Deposit deposit, Integer amount) {
        if (deposit == null || amount == 0) {
            throw new BadRequestException("Wrong deposit id or amount equals zero");
        }
        final Integer depositBalanceBefore = deposit.getCurDepositBalance();
        if (deposit.getCurDepositBalance() + amount > 0) {
            deposit.setCurDepositBalance(deposit.getCurDepositBalance() + amount);
        } else {
            throw new BadRequestException("Deposit balance can not be less than zero");
        }
        if (amount > 0) {
            return new Transaction(deposit, amount, depositBalanceBefore, OperationType.RECHARGE, new Date());
        }
        return new Transaction(deposit, amount, depositBalanceBefore, OperationType.WRITE_OFF, new Date());
    }
}
