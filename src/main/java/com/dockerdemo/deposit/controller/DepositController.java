package com.dockerdemo.deposit.controller;

import com.dockerdemo.deposit.domain.Deposit;
import com.dockerdemo.deposit.validator.InRange;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

import static org.apache.logging.log4j.LogManager.getLogger;

@RestController
@RequestMapping("/api/v1")
@Api(value = "Deposit Management")
public interface DepositController {

    Logger logger = getLogger(DepositController.class);

    @GetMapping("/deposit/{id}")
    @ApiOperation(value = "Get concrete deposit", response = Deposit.class)
    ResponseEntity<Deposit> getOneDeposit(
            @ApiParam(value = "Deposit id", required = true) @Valid @NonNull @PathVariable Integer id);

    @GetMapping("/deposit")
    @ApiOperation(value = "Get all deposits", response = Deposit.class, responseContainer = "List")
    ResponseEntity<List<Deposit>> getAllDeposits();

    @GetMapping("/deposit/all/{userId}")
    @ApiOperation(value = "Get all deposits by user id", response = Deposit.class, responseContainer = "List")
    ResponseEntity<List<Deposit>> getAllDepositsByUserId(
            @ApiParam(value = "User id", required = true) @Valid @NonNull @PathVariable(value = "userId") Integer userId);

    @PostMapping("/deposit")
    @ApiOperation(value = "Create deposit", response = String.class)
    ResponseEntity<String> createDeposit(
            @ApiParam(value = "Deposit id", required = true) @Valid @NonNull @RequestParam Integer userId,
            @ApiParam(value = "Deposit value", required = true) @Valid @NonNull @InRange @RequestParam Integer balance,
            UriComponentsBuilder builder);

    @DeleteMapping("/deposit/{id}")
    @ApiOperation(value = "Delete deposit", response = String.class)
    ResponseEntity<Void> deleteOneDeposit(
            @ApiParam(value = "Deposit id", required = true) @Valid @NonNull @PathVariable Integer id);

    @DeleteMapping("/deposit/all/{userId}")
    @ApiOperation(value = "Delete all user deposits")
    ResponseEntity<Void> deleteAllDepositsByUserId(
            @ApiParam(value = "User id", required = true) @Valid @NonNull @PathVariable(value = "userId") Integer userId);

    @DeleteMapping("/deposit/all")
    @ApiOperation(value = "Delete all user deposits")
    ResponseEntity<Void> deleteAllDeposits();
}