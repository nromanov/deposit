package com.dockerdemo.deposit.controller;

import com.dockerdemo.deposit.domain.User;
import com.dockerdemo.deposit.validator.InRange;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

import static org.apache.logging.log4j.LogManager.getLogger;

@RestController
@RequestMapping("/api/v1")
@Validated
@Api(value = "Users Management")
public interface UserController {

    Logger logger = getLogger(UserController.class);

    @GetMapping("/user")
    @ApiOperation(value = "Get all users", response = User.class, responseContainer = "List")
    ResponseEntity<List<User>> getAllUsers();

    @GetMapping("/user/{id}")
    @ApiOperation(value = "Get concrete user", response = User.class)
    ResponseEntity<User> getOneUser(
            @ApiParam(value = "User id", required = true) @Valid @NonNull @InRange @PathVariable Integer id);

    @PostMapping("/user")
    @ApiOperation(value = "Create user", response = String.class)
    ResponseEntity<String> createUser(
            @ApiParam(value = "User first name", required = true) @Valid @NonNull @RequestParam String firstName,
            @ApiParam(value = "User last name", required = true) @Valid @NonNull @RequestParam String lastName,
            UriComponentsBuilder builder);

    @PutMapping("/user/{id}")
    @ApiOperation(value = "Update user", response = String.class)
    ResponseEntity<String> updateUser(
            @ApiParam(value = "User id", required = true) @Valid @NonNull @PathVariable Integer id,
            @ApiParam(value = "User first name", required = true) @Valid @NonNull @RequestParam String firstName,
            @ApiParam(value = "User last name", required = true) @Valid @NonNull @RequestParam String lastName,
            UriComponentsBuilder builder);

    @DeleteMapping("/user/{id}")
    @ApiOperation(value = "Delete user", response = String.class)
    ResponseEntity<String> deleteOneUser(
            @ApiParam(value = "User id", required = true) @Valid @NonNull @PathVariable Integer id);

    @DeleteMapping("/user/all")
    @ApiOperation(value = "Delete all users")
    ResponseEntity<String> deleteAllUsers();
}