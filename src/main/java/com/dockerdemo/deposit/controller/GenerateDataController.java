package com.dockerdemo.deposit.controller;

import com.dockerdemo.deposit.validator.InRange;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static org.apache.logging.log4j.LogManager.getLogger;

@RestController
@RequestMapping("/api/v1")
@Api(value = "Deposit Management")
public interface GenerateDataController {

    Logger logger = getLogger(GenerateDataController.class);

    @PostMapping("/generate")
    @ApiOperation(value = "Generate data", response = String.class)
    ResponseEntity<String> generateData(
            @ApiParam(value = "User count", required = true)
                @Valid @InRange @NonNull @RequestParam Integer userCount,
            @ApiParam(value = "Min deposit count per user")
                @Valid @InRange @RequestParam(defaultValue = "0") Integer minDepCount,
            @ApiParam(value = "Max deposit count per user")
                @Valid @InRange @RequestParam(defaultValue = "3") Integer maxDepCount,
            @ApiParam(value = "Min transaction count per deposit")
                @Valid @InRange @RequestParam(defaultValue = "0") Integer minTrCount,
            @ApiParam(value = "Max transaction count per deposit")
                @Valid @InRange @RequestParam(defaultValue = "5") Integer maxTrCount
    );
}