package com.dockerdemo.deposit.controller;

import com.dockerdemo.deposit.domain.Transaction;
import com.dockerdemo.deposit.validator.InRange;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.apache.logging.log4j.LogManager.getLogger;

@RestController
@Validated
@RequestMapping("/api/v1")
@Api(value = "Transaction Management")
public interface TransactionController {

    Logger logger = getLogger(TransactionController.class);

    @GetMapping("/transaction/{id}")
    @ApiOperation(value = "Get concrete transaction", response = Transaction.class)
    ResponseEntity<Transaction> getOneTransaction(
            @ApiParam(value = "Transaction id", required = true) @Valid @NonNull @PathVariable UUID id);

    @GetMapping("/transaction")
    @ApiOperation(value = "Get all transactions", response = Transaction.class, responseContainer = "List")
    ResponseEntity<List<Transaction>> getAllTransactions();

    @GetMapping("/transaction/all/{depositId}")
    @ApiOperation(value = "Get all transaction by deposit id", response = Transaction.class, responseContainer = "List")
    ResponseEntity<Set<Transaction>> getAllTransactionsByDepositId(
            @ApiParam(value = "Deposit id", required = true) @Valid @NonNull @PathVariable(value = "depositId") Integer depositId);

    @PostMapping("/transaction")
    @ApiOperation(value = "Create transaction", response = String.class)
    ResponseEntity<String> createTransaction(
            @ApiParam(value = "Deposit id", required = true) @Valid @NonNull @RequestParam(name = "depositId") Integer depositId,
            @ApiParam(value = "Transaction amount", required = true) @Valid @InRange @RequestParam(name = "amount") Integer amount,
            UriComponentsBuilder builder);
}