package com.dockerdemo.deposit.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Data
@Entity
@Table(name = "Deposits")
@ApiModel(description = "Deposit details")
public class Deposit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(notes = "The database generated deposit ID")
    @Column(name = "deposit_id")
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = User.class)
    @ApiModelProperty(notes = "The user entity")
    @JoinColumn(name = "user_id")
    @JsonBackReference
    private User user;

    @Column(name = "balance_value")
    @ApiModelProperty(notes = "Balance value")
    private Integer curDepositBalance;

    @Column(name = "deposit_open_date")
    @Type(type = "timestamp")
    @ApiModelProperty(notes = "Deposit open date")
    private Date dateTime;

    @OneToMany(fetch = FetchType.EAGER, targetEntity = Transaction.class, mappedBy = "deposit")
    @JsonManagedReference
    private Set<Transaction> transactions = new HashSet<>();

    public Deposit() {
    }

    public Deposit(User user, Integer balance, Date dateTime) {
        this.user = user;
        this.curDepositBalance = balance;
        this.dateTime = dateTime;
    }

    public void setUser(User user) {
        user.getDeposits().add(this);
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Deposit deposit = (Deposit) o;
        return Objects.equals(id, deposit.id) &&
                Objects.equals(curDepositBalance, deposit.curDepositBalance) &&
                Objects.equals(transactions, deposit.transactions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, curDepositBalance, dateTime, transactions);
    }

}
