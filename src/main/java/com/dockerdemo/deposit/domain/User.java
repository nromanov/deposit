package com.dockerdemo.deposit.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "Users")
@ApiModel(description = "User details")
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    @ApiModelProperty(notes = "The database generated user ID")
    private Integer id;

    @Column(name = "first_name")
    @ApiModelProperty(notes = "The user first name")
    @NotEmpty
    private String firstName;

    @Column(name = "last_name")
    @ApiModelProperty(notes = "The user last name")
    @NotEmpty
    private String lastName;

    @OneToMany(fetch = FetchType.EAGER, targetEntity = Deposit.class, mappedBy = "user")
    @JsonManagedReference
    private Set<Deposit> deposits = new HashSet<>();

    public User() {
    }

    public User(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(deposits, user.deposits);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName);
    }
}
