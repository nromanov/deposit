package com.dockerdemo.deposit.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "Transactions")
@ApiModel(description = "Transaction details")
@Getter
@Setter
public class Transaction {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @ColumnDefault("random_uuid()")
    @Type(type = "uuid-char")
    @Column(name = "transaction_id", updatable = false)
    @ApiModelProperty(notes = "The database generated transaction ID")
    private UUID id;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = Deposit.class)
    @JoinColumn(name = "deposit_id")
    @ApiModelProperty(notes = "Deposit entity")
    @JsonBackReference
    private Deposit deposit;

    @Column(name = "transaction_value")
    @ApiModelProperty(notes = "Transaction amount")
    private Integer transactionAmount;

    @Column(name = "transaction_type")
    @Enumerated(EnumType.STRING)
    private OperationType operation;

    @Column(name = "transaction_date")
    @Type(type = "timestamp")
    @ApiModelProperty(notes = "Transaction timestamp")
    private Date dateTime;

    @Column(name = "depositBefore")
    @ApiModelProperty(notes = "Deposit balance before transaction")
    private Integer depositBefore;

    public Transaction() {
    }

    public Transaction(Deposit deposit, Integer transaction_amount, Integer depositBefore, OperationType type, Date dateTime) {
        this.deposit = deposit;
        this.transactionAmount = transaction_amount;
        this.dateTime = dateTime;
        this.operation = type;
        this.depositBefore = depositBefore;
    }

    public void setDeposit(Deposit deposit) {
        deposit.getTransactions().add(this);
        this.deposit = deposit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return id.equals(that.id) &&
                transactionAmount.equals(that.transactionAmount) &&
                operation == that.operation &&
                depositBefore.equals(that.depositBefore);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, transactionAmount, operation, dateTime, depositBefore);
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", transactionAmount=" + transactionAmount +
                ", operation=" + operation +
                ", dateTime=" + dateTime +
                ", depositBefore=" + depositBefore +
                '}';
    }
}
