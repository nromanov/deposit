package com.dockerdemo.deposit.domain;

public enum OperationType {
    RECHARGE,
    WRITE_OFF
}
