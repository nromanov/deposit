package com.dockerdemo.deposit.repo;

import com.dockerdemo.deposit.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepo extends JpaRepository<User, Long> {
    List<User> findAll();
    User findById(Integer id);
}
