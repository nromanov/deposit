package com.dockerdemo.deposit.repo;

import com.dockerdemo.deposit.domain.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface TransactionRepo extends JpaRepository<Transaction, Long> {
    List<Transaction> findAll();
    Transaction findById(UUID id);
    Set<Transaction> findAllByDepositId(Integer depositId);
}
