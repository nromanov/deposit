package com.dockerdemo.deposit.repo;

import com.dockerdemo.deposit.domain.Deposit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DepositRepo extends JpaRepository<Deposit, Long> {
    List<Deposit> findAll();
    List<Deposit> findAllByUserId(Integer id);
    Deposit findById(Integer id);
}
