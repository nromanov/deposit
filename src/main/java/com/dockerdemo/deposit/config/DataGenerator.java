package com.dockerdemo.deposit.config;

import com.dockerdemo.deposit.domain.Deposit;
import com.dockerdemo.deposit.domain.OperationType;
import com.dockerdemo.deposit.domain.Transaction;
import com.dockerdemo.deposit.domain.User;
import com.github.javafaker.Faker;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Component
public class DataGenerator {

    private static Faker faker = new Faker(Locale.US);
    private Set<User> users = new HashSet<>();
    private SortedSet<Deposit> deposits = new TreeSet<>(Comparator.comparing(Deposit::getDateTime));
    private SortedSet<Transaction> transactions = new TreeSet<>(Comparator.comparing(Transaction::getDateTime));
    public static ZoneId zoneId = ZoneId.of("UTC");

    public Map<String, Object> generateData(int userCount, int minDep, int maxDep, int minTr, int maxTr) {
        for(int i=0; i<userCount; i++) {
            User user = createUser();
            users.add(user);
            int rdmDepositCount = faker.number().numberBetween(minDep, maxDep);
            for(int j=0; j<rdmDepositCount; j++) {
                Deposit deposit = createDeposit(user);
                deposits.add(deposit);
                int rdmTransactionCount = faker.number().numberBetween(minTr, maxTr);
                for(int k=1; k<=rdmTransactionCount; k++) {
                    Transaction transaction = createTransaction(deposit, k);
                    deposit.setCurDepositBalance(deposit.getCurDepositBalance() + transaction.getTransactionAmount());
                    transactions.add(transaction);
                }
                deposit.setTransactions(transactions);
            }
            user.setDeposits(deposits);
        }
        return new HashMap<String, Object>() {{
            put("users", users);
            put("deposits", deposits);
            put("transactions", transactions);
        }};
    }

    private static Date getNextTransactionDate(Date date, int days) {
        LocalDateTime localDateTime = date.toInstant().atZone(zoneId).toLocalDateTime();
        localDateTime = localDateTime.plusDays(days);
        return formatDate(Date.from(localDateTime.atZone(zoneId).toInstant()));
    }

    public static Date formatDate(Date date) {
        SimpleDateFormat frm = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        frm.setTimeZone(TimeZone.getTimeZone(zoneId));
        frm.format(date);
        return date;
    }

    public static User createUser() {
        return new User(faker.name().firstName(), faker.name().lastName());
    }

    public static Deposit createDeposit(User user) {
        return new Deposit(user, faker.number().numberBetween(1000, 5000),
                formatDate(faker.date().between(
                        Date.from(LocalDateTime.now().minusMonths(6).atZone(zoneId).toInstant()),
                        Date.from(LocalDateTime.now().atZone(zoneId).toInstant()))));
    }

    public static Transaction createTransaction(Deposit deposit, Integer trCount) {
        int trAmount = faker.number().numberBetween(-50, 50);
        return new Transaction(deposit, trAmount == 0 ? ++trAmount : trAmount,
                deposit.getCurDepositBalance(), trAmount > 0 ?
                OperationType.RECHARGE : OperationType.WRITE_OFF,
                getNextTransactionDate(deposit.getDateTime(), trCount));
    }
}