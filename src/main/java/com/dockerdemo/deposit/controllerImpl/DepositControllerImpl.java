package com.dockerdemo.deposit.controllerImpl;

import com.dockerdemo.deposit.controller.DepositController;
import com.dockerdemo.deposit.domain.Deposit;
import com.dockerdemo.deposit.domain.User;
import com.dockerdemo.deposit.error.BadRequestException;
import com.dockerdemo.deposit.error.NotFoundException;
import com.dockerdemo.deposit.repo.DepositRepo;
import com.dockerdemo.deposit.repo.TransactionRepo;
import com.dockerdemo.deposit.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class DepositControllerImpl implements DepositController {

    @Autowired
    private DepositRepo depositRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private TransactionRepo transactionRepo;

    @Override
    public ResponseEntity<Deposit> getOneDeposit(Integer id) {
        Deposit deposit = getDeposit(id);
        return ResponseEntity.ok().body(deposit);
    }

    @Override
    public ResponseEntity<List<Deposit>> getAllDeposits() {
        List<Deposit> depositList = depositRepo.findAll();
        return ResponseEntity.ok().body(depositList);
    }

    @Override
    public ResponseEntity<List<Deposit>> getAllDepositsByUserId(Integer id) {
        List<Deposit> depositList = depositRepo.findAllByUserId(id);
        return ResponseEntity.ok().body(depositList);
    }

    @Override
    public ResponseEntity<String> createDeposit(Integer userId, Integer balance,
                                                UriComponentsBuilder builder) {
        User user = userRepo.findById(userId);
        if (user == null) {
            throw new BadRequestException(String.format("User with id: %s not found", userId));
        }
        Deposit deposit = new Deposit(user, balance, new Date());
        depositRepo.save(deposit);
        return getResponseEntityWithUri(deposit, builder);
    }

    @Override
    public ResponseEntity<Void> deleteOneDeposit(Integer id) {
        Deposit deposit = getDeposit(id);
        if (transactionRepo.findAllByDepositId(id) == null || transactionRepo.findAllByDepositId(id).isEmpty()) {
            depositRepo.delete(deposit);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        }
        throw new BadRequestException("Can not delete deposit with exist transactions");
    }

    @Override
    public ResponseEntity<Void> deleteAllDepositsByUserId(Integer userId) {
        Set<Deposit> depositsUserRepo = userRepo.findById(userId).getDeposits();
        List<Deposit> depositsDepositRepo = depositRepo.findAllByUserId(userId);
        if (depositsUserRepo.size() != depositsDepositRepo.size() ||
                !depositsUserRepo.containsAll(depositsDepositRepo)) {
            throw new BadRequestException("Deposit count (UserRepo, DepositRepo) is not equal for userId: " + userId);
        } else {
            return deleteAllDeposits(depositsDepositRepo);
        }
    }

    @Override
    public ResponseEntity<Void> deleteAllDeposits() {
        return deleteAllDeposits(depositRepo.findAll());
    }

    private ResponseEntity<Void> deleteAllDeposits(List<Deposit> deposits) {
        List<Deposit> depositsWithNoTransactions = deposits.stream()
                .filter(i -> transactionRepo.findAllByDepositId(i.getId()).isEmpty())
                .collect(Collectors.toList());
        if (!depositsWithNoTransactions.isEmpty()) {
            depositRepo.deleteAll(depositsWithNoTransactions);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        }
        throw new NotFoundException("No deposit without transactions found");
    }

    private Deposit getDeposit(Integer id) {
        Deposit deposit = depositRepo.findById(id);
        if (deposit == null) {
            throw new NotFoundException("No deposit found with id: " + id);
        }
        return deposit;
    }

    private ResponseEntity<String> getResponseEntityWithUri(Deposit deposit,
                                                            UriComponentsBuilder builder) {
        UriComponents uriComponents =
                builder.path("/deposit/{id}").buildAndExpand(deposit.getId());

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(uriComponents.toUri());
        headers.set("depositId", String.valueOf(deposit.getId()));
        return new ResponseEntity<>("New deposit was created with id: " + deposit.getId(),
                headers, HttpStatus.CREATED);
    }
}