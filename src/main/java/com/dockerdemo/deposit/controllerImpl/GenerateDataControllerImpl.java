package com.dockerdemo.deposit.controllerImpl;

import com.dockerdemo.deposit.config.DataGenerator;
import com.dockerdemo.deposit.controller.GenerateDataController;
import com.dockerdemo.deposit.domain.Deposit;
import com.dockerdemo.deposit.domain.Transaction;
import com.dockerdemo.deposit.domain.User;
import com.dockerdemo.deposit.repo.DepositRepo;
import com.dockerdemo.deposit.repo.TransactionRepo;
import com.dockerdemo.deposit.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Set;

@RestController
public class GenerateDataControllerImpl implements GenerateDataController {

    @Autowired
    private DepositRepo depositRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private TransactionRepo transactionRepo;

    @Override
    @SuppressWarnings("unchecked")
    public ResponseEntity<String> generateData(
            Integer userCount, Integer minDepCount, Integer maxDepCount,
            Integer minTrCount, Integer maxTrCount) {

        Map<String, Object> data = new DataGenerator().generateData(
                userCount, minDepCount, maxDepCount, minTrCount, maxTrCount);

        userRepo.saveAll((Set<User>)data.get("users"));
        depositRepo.saveAll((Set<Deposit>)data.get("deposits"));
        transactionRepo.saveAll((Set<Transaction>)data.get("transactions"));
        return new ResponseEntity<>("Data was generated", HttpStatus.CREATED);
    }
}