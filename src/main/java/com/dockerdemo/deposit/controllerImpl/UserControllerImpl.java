package com.dockerdemo.deposit.controllerImpl;

import com.dockerdemo.deposit.controller.UserController;
import com.dockerdemo.deposit.domain.User;
import com.dockerdemo.deposit.error.BadRequestException;
import com.dockerdemo.deposit.error.NotFoundException;
import com.dockerdemo.deposit.repo.DepositRepo;
import com.dockerdemo.deposit.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class UserControllerImpl implements UserController {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private DepositRepo depositRepo;

    @Override
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> users = userRepo.findAll();
        return ResponseEntity.ok().body(users);
    }

    @Override
    public ResponseEntity<User> getOneUser(Integer id) {
        User user = getUser(id);
        return ResponseEntity.ok().body(user);
    }

    @Override
    public ResponseEntity<String> createUser(String firstName, String lastName,
                                             UriComponentsBuilder builder) {
        User user = new User(firstName, lastName);
        userRepo.save(user);
        return getResponseEntityWithUri(user, builder);
    }

    @Override
    public ResponseEntity<String> updateUser(Integer id, String firstName, String lastName,
                                             UriComponentsBuilder builder) {
        User user = getUser(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        userRepo.save(user);
        return new ResponseEntity<>("User was updated", HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> deleteOneUser(Integer id) {
        User user = getUser(id);
        if (user.getDeposits().isEmpty()) {
            userRepo.delete(user);
            return new ResponseEntity<>("User was deleted: " + user, HttpStatus.ACCEPTED);
        }
        throw new BadRequestException("Can not delete user with exist deposits");
    }

    @Override
    public ResponseEntity<String> deleteAllUsers() {
        List<User> usersWithNoDeposit = userRepo.findAll().stream()
                .filter(i -> i.getDeposits().isEmpty())
                .collect(Collectors.toList());
        if (!usersWithNoDeposit.isEmpty()) {
            userRepo.deleteAll(usersWithNoDeposit);
            String ids = usersWithNoDeposit.stream()
                    .map(i -> String.valueOf(i.getId()))
                    .collect(Collectors.joining(", "));
            return new ResponseEntity<>("Users were delete: " + ids, HttpStatus.ACCEPTED);
        }
        throw new NotFoundException("No users without deposit found");
    }

    private User getUser(Integer id) {
        User user = userRepo.findById(id);
        if (user == null) {
            throw new NotFoundException("No user found with id: " + id);
        }
        return user;
    }

    private ResponseEntity<String> getResponseEntityWithUri(User user, UriComponentsBuilder builder) {
        UriComponents uriComponents =
                builder.path("/user/{id}").buildAndExpand(user.getId());

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(uriComponents.toUri());
        headers.set("userId", String.valueOf(user.getId()));
        return new ResponseEntity<>("New user was created with id: " + user.getId(),
                headers, HttpStatus.CREATED);
    }
}