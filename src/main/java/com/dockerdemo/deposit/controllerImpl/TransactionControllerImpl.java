package com.dockerdemo.deposit.controllerImpl;

import com.dockerdemo.deposit.controller.TransactionController;
import com.dockerdemo.deposit.counting.TransactionOperations;
import com.dockerdemo.deposit.domain.Deposit;
import com.dockerdemo.deposit.domain.Transaction;
import com.dockerdemo.deposit.error.NotFoundException;
import com.dockerdemo.deposit.repo.DepositRepo;
import com.dockerdemo.deposit.repo.TransactionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@RestController
public class TransactionControllerImpl implements TransactionController {

    @Autowired
    private TransactionRepo transactionRepo;

    @Autowired
    private DepositRepo depositRepo;

    @Override
    public ResponseEntity<Transaction> getOneTransaction(UUID id) {
        Transaction transaction = getTransaction(id);
        return ResponseEntity.ok().body(transaction);
    }

    @Override
    public ResponseEntity<List<Transaction>> getAllTransactions() {
        List<Transaction> transactionList = transactionRepo.findAll();
        return ResponseEntity.ok().body(transactionList);
    }

    @Override
    public ResponseEntity<Set<Transaction>> getAllTransactionsByDepositId(Integer depositId) {
        Set<Transaction> transactionList = transactionRepo.findAllByDepositId(depositId);
        return ResponseEntity.ok().body(transactionList);
    }

    @Override
    public ResponseEntity<String> createTransaction(Integer depositId, Integer amount,
                                                         UriComponentsBuilder builder) {
        Deposit deposit = depositRepo.findById(depositId);
        Transaction transaction = TransactionOperations.createTransaction(deposit, amount);
        depositRepo.save(deposit);
        transactionRepo.save(transaction);
        return getResponseEntityWithUri(transaction, HttpStatus.CREATED, builder);
    }

    private Transaction getTransaction(UUID id) {
        Transaction transaction = transactionRepo.findById(id);
        if (transaction == null) {
            throw new NotFoundException("No transaction found with id: " + id);
        }
        return transaction;
    }

    private ResponseEntity<String> getResponseEntityWithUri(Transaction transaction, HttpStatus status,
                                                                 UriComponentsBuilder builder) {
        UriComponents uriComponents =
                builder.path("/transaction/{id}").buildAndExpand(transaction.getId());

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(uriComponents.toUri());
        headers.set("transactionId", String.valueOf(transaction.getId()));
        return new ResponseEntity<>("New deposit was created with id: " + transaction.getId(),
                headers, HttpStatus.CREATED);
    }
}
