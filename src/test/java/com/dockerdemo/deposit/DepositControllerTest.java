package com.dockerdemo.deposit;

import com.dockerdemo.deposit.domain.Deposit;
import com.dockerdemo.deposit.domain.User;
import io.qameta.allure.Feature;
import org.springframework.http.HttpStatus;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@Feature("Deposition controller tests")
public class DepositControllerTest extends BaseTest {

    @Test(description = "Create deposit")
    public void createDeposit() {
        User user = createNewUser();
        Integer depositId = Integer.parseInt(
                given().spec(rSpec())
                        .queryParam("userId", user.getId())
                        .queryParams("balance", faker.number().randomDigitNotZero())
                        .when().post(DEPOSIT_URL)
                        .then().statusCode(HttpStatus.CREATED.value())
                        .extract().header("depositId"));

        assertThat(depositRepo.findById(depositId), is(notNullValue()));
    }

    @Test(description = "Get deposit")
    public void getDeposit() {
        Deposit expectedDeposit = createNewDeposit();
        Deposit actualDeposit = given().spec(rSpec())
                .pathParam("id", expectedDeposit.getId())
                .get(DEPOSIT_URL + "{id}")
                .then().statusCode(HttpStatus.OK.value())
                .extract().body().as(Deposit.class);

        assertThat(actualDeposit, is(equalTo(expectedDeposit)));
    }

   @Test(description = "Delete user")
    public void deleteDeposit() {
       Deposit deposit = createNewDeposit();
        given().spec(rSpec())
                .pathParam("id", deposit.getId())
                .when().delete(DEPOSIT_URL + "{id}")
                .then().statusCode(HttpStatus.ACCEPTED.value());

       assertThat(depositRepo.findById(deposit.getId()), is(nullValue()));
    }
}