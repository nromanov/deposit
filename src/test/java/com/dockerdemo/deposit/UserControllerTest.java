package com.dockerdemo.deposit;

import com.dockerdemo.deposit.domain.User;
import io.qameta.allure.Feature;
import org.springframework.http.HttpStatus;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@Feature("User controller tests")
public class UserControllerTest extends BaseTest {

    @Test(description = "Create user")
    public void createUser() {
        Integer userId = Integer.parseInt(given().spec(rSpec())
                .queryParams(getParams())
                .post(USER_URL)
                .then().statusCode(HttpStatus.CREATED.value())
                .extract().header("userId"));

        assertThat(userRepo.findById(userId), is(notNullValue()));
    }

    @Test(description = "Get concrete user")
    public void getUser() {
        User expectedUser = createNewUser();
        User actualUser = given().spec(rSpec())
                .pathParam("id", expectedUser.getId())
                .get(USER_URL + "{id}")
                .then().statusCode(HttpStatus.OK.value())
                .extract().body().as(User.class);

        assertThat(actualUser, is(equalTo(expectedUser)));
    }

    @Test(description = "Update user")
    public void updateUser() {
        User userBefore = createNewUser();
        given().spec(rSpec())
                .queryParams(getParams())
                .pathParam("id", userBefore.getId())
                .when().put(USER_URL + "{id}")
                .then().statusCode(HttpStatus.OK.value());

        assertThat(userBefore, not(equalTo(userRepo.findById(userBefore.getId()))));
    }

    @Test(description = "Delete user")
    public void deleteUser() {
        User user = createNewUser();
        given().spec(rSpec())
                .pathParam("id", user.getId())
                .when().delete(USER_URL + "{id}")
                .then().statusCode(HttpStatus.ACCEPTED.value());

        assertThat(userRepo.findById(user.getId()), is(nullValue()));
    }
}