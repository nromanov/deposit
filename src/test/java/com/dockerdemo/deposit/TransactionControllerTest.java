package com.dockerdemo.deposit;

import com.dockerdemo.deposit.domain.Deposit;
import com.dockerdemo.deposit.domain.Transaction;
import io.qameta.allure.Feature;
import org.springframework.http.HttpStatus;
import org.testng.annotations.Test;

import java.util.List;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@Feature("Transaction controller tests")
public class TransactionControllerTest extends BaseTest {

    @Test(description = "Create transaction")
    public void createTransaction() {

        Deposit deposit = createNewDeposit();

        Integer depositBalance = deposit.getCurDepositBalance();
        Integer transactionAmount = faker.number().randomDigitNotZero();
        Integer expectedBalance = depositBalance + transactionAmount;

        UUID transactionId = UUID.fromString(
                given().spec(rSpec())
                        .queryParam("depositId", deposit.getId())
                        .queryParams("amount", transactionAmount)
                        .when().post(TRANSACTION_URL)
                        .then().statusCode(HttpStatus.CREATED.value())
                        .extract().header("transactionId"));

        assertThat(depositRepo.findById(deposit.getId()).getCurDepositBalance(),
                is(equalTo(expectedBalance)));

        assertThat(transactionRepo.findById(transactionId), allOf(
                hasProperty("transactionAmount", is(equalTo(transactionAmount))),
                hasProperty("depositBefore", is(equalTo(depositBalance)))
        ));
    }

    @Test(description = "Get transactions")
    public void getAllTransactions() {

        Transaction transaction = createNewTransaction();
        List<Transaction> transactions = given().spec(rSpec())
                .when().get(TRANSACTION_URL)
                .then().statusCode(HttpStatus.OK.value())
                .extract().body().jsonPath().getList(".", Transaction.class);

        assertThat("Transaction is not in list", transactions.contains(transaction));
    }
}