package com.dockerdemo.deposit;

import com.dockerdemo.deposit.config.DataGenerator;
import com.dockerdemo.deposit.domain.Deposit;
import com.dockerdemo.deposit.domain.Transaction;
import com.dockerdemo.deposit.domain.User;
import com.dockerdemo.deposit.repo.DepositRepo;
import com.dockerdemo.deposit.repo.TransactionRepo;
import com.dockerdemo.deposit.repo.UserRepo;
import com.github.javafaker.Faker;
import config.RequestListener;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Listeners;

import java.util.HashMap;
import java.util.Map;


@SpringBootTest(classes = {DepositApplication.class},
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Listeners(RequestListener.class)
public abstract class BaseTest extends AbstractTestNGSpringContextTests {

    public final static String DEPOSIT_URL = "/deposit/";
    public final static String USER_URL = "/user/";
    public final static String TRANSACTION_URL = "/transaction/";

    public static final Faker faker = new Faker();

    @Autowired
    UserRepo userRepo;

    @Autowired
    DepositRepo depositRepo;

    @Autowired
    TransactionRepo transactionRepo;

    RequestSpecification rSpec() {
        return new RequestSpecBuilder()
                .setBasePath("/api/v1")
                .setContentType(ContentType.JSON)
                .setAccept(ContentType.JSON)
                .build();
    }

    public User createNewUser() {
        User user = DataGenerator.createUser();
        userRepo.save(user);
        return user;
    }

    public Deposit createNewDeposit() {
        User user = createNewUser();
        Deposit deposit = DataGenerator.createDeposit(user);
        depositRepo.save(deposit);
        return deposit;
    }

    public Transaction createNewTransaction() {
        Deposit deposit = createNewDeposit();
        Transaction transaction = DataGenerator.createTransaction(deposit, 1);
        transactionRepo.save(transaction);
        return transaction;
    }

    public Map<String, Object> getParams() {
        return new HashMap<String, Object>() {{
            put("firstName", faker.name().firstName());
            put("lastName", faker.name().lastName());
        }};
    }
}