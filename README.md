# Deposit service

Simple Spring CRUD service.

User table - UserController

Deposit table - DepositController

Transaction table - TransactionController

Generate Data - GenerateDataController

One user can have many deposit. One deposit can have many transactions.

## Getting Started

Please, run DepositApplication.class to up and run service.
You can change port in application.properties.
```
server.port:8080 
```
### Tests

To run test you can use *all.xml* testng config. It will up and run
service before test will start. You can run service and tests separately.
Todo this comment in BaseTest.class: 

```
@SpringBootTest(classes = {DepositApplication.class},
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
```
### Reporting

To see test run result Allure report is available.
Please run from project pom directory:

```
mvn io.qameta.allure:allure-maven:report
```

### Api

Swagger is available after service is up and running by link:

```
http://localhost:8080/swagger-ui.html#/
```

### H2 DB config

H2 used with in-memory mode. Can be changed in application.properties

```
spring.datasource.url = jdbc:h2:file:/data/sample
```

### H2 console

H2 console is available after service is up and running by link 

```
http://localhost:8080/h2-console
```





